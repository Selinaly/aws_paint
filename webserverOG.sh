#!/bin/bash
if (( $# > 0 ))
then
  hostname=$1
else
  echo "WTF: you must supply a hostname or IP address" 1>&2
  exit 1
fi

scp -o StrictHostKeyChecking=no -i ~/.ssh/SelinaLyKey.pem jspaint.init ec2-user@$hostname:jspaint.init

ssh -o StrictHostKeyChecking=no -i ~/.ssh/SelinaLyKey.pem ec2-user@$hostname '
sudo yum -y install httpd
if rpm -qa | grep "^httpd-[0-9]" >/dev/null 2>&1
then
  sudo systemctl start httpd
else 
  exit 1

fi

sudo yum -y install git

git clone https://github.com/1j01/jspaint.git

cd ~/jspaint
VERSION=v14.16.1
DISTRO=linux-x64
sudo mkdir -p /usr/local/lib/nodejs
wget https://nodejs.org/dist/$VERSION/node-$VERSION-$DISTRO.tar.xz 
sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs 

echo "
# Nodejs
VERSION=v14.16.1 
DISTRO=linux-x64 
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH" >> ~/.bash_profile 
source ~/.bash_profile
cd ~/jspaint
npm i 


cd /etc/httpd/conf.d
sudo sh -c "echo \"<VirtualHost *:80>
    ProxyPreserveHost On
    
    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
</VirtualHost>\" >>jspaint.conf"
sudo systemctl restart httpd

sudo mv ~/jspaint.init /etc/init.d/jspaint
sudo chmod +x /etc/init.d/jspaint
sudo chkconfig --add jspaint
sudo systemctl enable jspaint 
sudo systemctl start jspaint
'