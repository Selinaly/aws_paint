# Setting up AWS Machine ec2 with JSPaint

In this project we will make a bash script to provision a machine in AWS to run JSPaint

## Subjects Covered

This project will cover:
1. Git
2. Bitbucket
3. Markdown
4. AWS 
   - ec2 creation
   - Security groups
5. Redhat 
   - installation 
   - script 
6. BASH
   - ssh
   - scp
   - heredoc
   - Outputs (stdin, stdout, stderr)
   - piping and grep

## Explaination of Commands

### Installing JSpaint on to AWS Webserver 

1. Install git
``` 
$ sudo yum -y install git
```
2. Clone jspaint
```
$ git clone https://github.com/1j01/jspaint.git
$ cd jspaint
```
3. Install nodejs
```
$ VERSION=v14.16.1
$ DISTRO=linux-x64
$ sudo mkdir -p /usr/local/lib/nodejs
$ wget https://nodejs.org/dist/v14.16.1/node-v14.16.1-linux-x64.tar.xz 
$ sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs 
```

```
$ echo '# Nodejs 
VERSION=v14.16.1 
DISTRO=linux-x64 
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH' >>./bash_profile 
source ~/.bash_profile

$ node -v (checks if the right version is installed)
$ npm i ./bash_profile (installs dependencies)
```

4. Reverse Proxy
```
$ cd /etc/httpd/conf.d
$ touch jspaint.conf
$ echo '<VirtualHost *:80>
    ProxyPreserveHost On

    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
</VirtualHost>' > jspaint.conf
$ sudo systemctl restart httpd
$ cd jspaint/
$ npm i (install dependancies)
$ sudo sh -c "cat > /etc/init.d/jspaint <<_end

5. JSPaint.init

#!/bin/bash
#description: JSPaint web app
#chkconfig: 2345 99 99
case $1 in
	'start')
		cd /home/ec2-user/jspaint
		nohup npm run dev &
		;;
	'stop')
		kill -9 $(ps -ef | egrep 'npm|node' | awk '{print $2}')
		;;
esac
_end"

$ sudo chmod +x /etc/init.d/jspaint
$ sudo chkconfig --add jspaint
$ sudo systemctl enable jspaint 
$ sudo systemctl start jspaint

```
run your webserver and check


